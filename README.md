<div align="center"><a href="https://larapp.com" target="_blank" align="center"><img src="https://gitlab.com/florent.lk/larapp/-/raw/main/logo.svg" width="400" alt="LarApp Logo"></a></div>

## À propos de LarApp
LarApp est un framework d'application Web doté d'une environement complet. Nous sommes fermement convaincus que le développement doit être une expérience agréable et créative pour être réellement épanouissante. Larapp vise à simplifier le processus de développement en automatisant les tâches couramment utilisées dans de nombreux projets d'applications Web, vous permettant ainsi de vous concentrer sur les fonctionnalités métier.

En utilisant l'écosystème complet de Laravel ainsi que des outils populaires, LarApp offre une base solide et moderne pour votre application.

## Apprendre LarApp
LarApp possède la [documentation](https://larapp.com/docs) 

## Roadmap
- [ ] Intégration Sail (Expérience de développement local Laravel artisanale à l'aide de Docker..)
- [ ] Intégration Octane (Boostez les performances de votre application en la conservant en mémoire.)
- [ ] Intégration Sanctum (Authentification des API et des applications mobiles sans vouloir s'arracher les cheveux.)
- [ ] Intégration Télescope (Déboguez votre application à l'aide de notre interface utilisateur de débogage et d'analyse.)
- [ ] Intégration Pint (Opinionated PHP code style fixer for minimalists.)
- [ ] Intégration Dusk (Tests de navigateur automatisés pour expédier votre application en toute confiance.)
- [ ] Intégration Horizon (Belle interface utilisateur pour surveiller vos files d'attente Laravel pilotées par Redis.)
- [ ] Intégration Envoy (configurer des tâches pour le déploiement)
- [ ] Intégration primevue ( théme interface)
- [ ] Intégration Precognition ( validation des données vue)
- [ ] Intégration sytème de module ( théme interface)



## Contribuer
Merci d'avoir envisagé de contribuer au framework LarApp ! Le guide de contribution se trouve dans la [documentation LarApp](https://larapp.com/docs/contributions) .

## Code de conduite
Afin de vous assurer que la communauté LarApp est accueillante pour tous, veuillez lire et respecter le [Code de conduite](https://larapp.com/docs/contributions#code-of-conduct) .

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## License
Le framework LarApp est un logiciel open source sous [licence MIT](https://opensource.org/license/mit/) .

